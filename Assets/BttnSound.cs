﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class BttnSound : MonoBehaviour, IPointerClickHandler,IPointerEnterHandler{
    AudioSource highlightSound;
    Button button;
    bool soundOnce= true;
	// Use this for initialization
	void Start () {
        highlightSound = gameObject.GetComponent<AudioSource>();
        button = GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (gameObject == EventSystem.current.currentSelectedGameObject)
        {
            //Debug.Log(" we're selected! ");
            if (!highlightSound.isPlaying && soundOnce==true)
            {
                highlightSound.Play();
                soundOnce = false;
            }
        }
        else
        
        {
            soundOnce = true;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //throw new NotImplementedException();
        print("I got clicked");
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //throw new NotImplementedException();
        print("I got entered");
    }
}
