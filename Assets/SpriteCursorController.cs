﻿using UnityEngine;
using System.Collections;

public class SpriteCursorController : MonoBehaviour
{
    SpriteRenderer spriteRender;
    public Sprite[] curser;
    int animationStep = 0;
    int StepTarget = 0;
    bool firstEnter = true;
    AudioSource highlightSound;
    // Use this for initialization
    void Start()
    {
        spriteRender = gameObject.GetComponent<SpriteRenderer>();
        highlightSound = gameObject.GetComponent<AudioSource>();
    }


    public void SwitchCursor()
    {
        if (animationStep == 0) animationStep = 1;
        else animationStep = 0;

        animationStep = 0;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        Ray spriteCursorRay = new Ray(transform.position, transform.forward);
        RaycastHit SpriteCursorRaycastHit;
        Physics.Raycast(spriteCursorRay, out SpriteCursorRaycastHit);

        //transform.Rotate(new Vector3(0, 0, .25f));

        spriteRender.sprite = curser[animationStep];

        if (SpriteCursorRaycastHit.collider != null)
        {
            if (Cardboard.SDK.Triggered)
            {
                gameObject.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f);

                if (animationStep == 0) animationStep = 1;
                //else if (animationStep == 1) animationStep = 0;
                if (!IsInvoking("AnimationStepToTarget"))
                {
                    StepTarget = 0;
                    Invoke("AnimationStepToTarget", .25f);

                }
            }
            else if (firstEnter == true)
            {
                highlightSound.Play();
                firstEnter = false;
                gameObject.transform.localScale = gameObject.transform.localScale * (1.5f * 1.618f);//new Vector3(.012f, .012f, .012f);
            }
            else
            {
                //Invoke("ChangeHoverScale", .125f);
            }
            print(gameObject.transform.localScale.x);
        }
        else
        {
            firstEnter = true;
            if (gameObject.transform.localScale.x > 0.008405f)
            {
                gameObject.transform.localScale = new Vector3(0.008405f, 0.008405f, 0.008405f);
            }
            if (!IsInvoking("AnimationStepToTarget"))
            {
                StepTarget = 0;
                Invoke("AnimationStepToTarget", .0125f);
            }
        }
    }
    public void AnimationStepToTarget()
    {
        if (animationStep > StepTarget)
        {
            animationStep--;
        }
        if (animationStep < StepTarget)
        {
            animationStep++;
        }

    }
    public void ChangeHoverScale()
    {
        gameObject.transform.localScale = new Vector3(0.01359929f, 0.01359929f, 0.01359929f);
    }

}
